#include <iostream>
#include "stack.hpp"

void stack::push(char item)
{
    stk.push_back(item);
}

bool stack::isempty()
{
    return stk.empty();
}

char stack::pop()
{
    char last_elem = topelement();

    if (last_elem != -1)
    {
        stk.pop_back();
    }

    return last_elem;
}

int stack::display()
{
    if(isempty())
    {
        return -1;
    }
    else
    {
        std::cout<<"\nContents:\n";
        for(char curr_elem : stk)
            std::cout<<curr_elem<<" ";
    }

    return 0;
}

char stack::topelement()
{
    if (isempty())
    {
        return -1;
    }

    return stk.back();
}
