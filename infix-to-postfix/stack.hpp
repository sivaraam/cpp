#include <vector>

class stack
{
private:
    std::vector<char> stk;

public:
    void push(char item);
    bool isempty();
    char pop();
    int display();
    char topelement();
};
