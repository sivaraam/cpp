#include <iostream>
#include <string>
#include "stack.hpp"

int priority(char a)
{
    switch (a)
    {
    case '^':
        return 3;
    case '*':
    case '/':
        return 2;
    case '+':
    case '-':
        return 1;
    default:
        return -1;
    }
}

int main()
{
    stack s;

    std::string infix_expression, postfix_expression;

    std::cout<<"enter infix expression:\n";
    std::cin>>infix_expression;

    for (char expr_char : infix_expression)
    {
      switch(expr_char)
      {
      case ' ':
          break;
      case '(':
          s.push(expr_char);
          break;
      case ')':
          while(!s.isempty() && s.topelement() != '(')
          {
              postfix_expression.push_back(s.pop());
          }
          s.pop();
          break;
      case '*':
      case '/':
      case '+':
      case '-':
          while( priority(expr_char) <= priority(s.topelement()) )
              postfix_expression.push_back(s.pop());
          s.push(expr_char);
          break;
      default:
          postfix_expression.push_back(expr_char);
          break;
      }
    }

    while(!s.isempty())
       postfix_expression.push_back(s.pop());

    std::cout<<"Postfix: ";
    std::cout<<postfix_expression<<'\n';
    return 0;
}
